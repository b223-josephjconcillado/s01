<?php
// 2. In code.php, create a function named getFullAddress() that will take four arguments:
// - Country
// - City
// - Province
// - Specific Address (such as block, lot, or building name and room number).
function getFullAddress($country,$city,$province,$specificAddress){
    return "$specificAddress, $city, $province, $country";
}
// 4. Create another function named getLetterGrade() that uses conditional statements to output a letter representation of a given numerical grade:
//     - A+ (98 to 100)
//     - A (95 to 97)
//     - A- (92 to 94)
//     - B+ (89 to 91)
//     - B (86 to 88)
//     - B- (83 to 85)
//     - C+ (80 to 82)
//     - C (77 to 79)
//     - C- (75 to 76)
//     - D (75 below)
function getLetterGrade($grade){
    if($grade < 75) {
        return 'D';
    } else if($grade >= 75 && $grade <= 76) {
        return 'C-';
    } else if($grade >= 77 && $grade <= 79) {
        return 'C';
    } else if($grade >= 80 && $grade <= 82) {
        return 'C+';
    } else if($grade >= 83 && $grade <= 85) {
        return 'B-';
    } else if($grade >= 86 && $grade <= 88) {
        return 'B';
    } else if($grade >= 89 && $grade <= 91) {
        return 'B+';
    } else if($grade >= 92 && $grade <= 94) {
        return 'A-';
    } else if($grade >= 95 && $grade <= 97) {
        return 'A';
    } else if($grade >= 98 && $grade <= 100) {
        return 'A+';
    }
}

?>