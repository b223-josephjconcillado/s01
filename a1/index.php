<?php require_once './code.php' ;?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Full Address</h1>
    <!-- 3. Have this function return the concatenated arguments to result in a coherent address. -->
    <p><?php echo getFullAddress('Philippines','Quezon City','Metro Manila','3F Caswynn Bldg., Timog Avenue');?></p>
    <p><?php echo getFullAddress('Philippines','Makati City','Metro Manila','3F Enzo Bldg., Buendia Avenue');?></p>

    <h1>Letter-Based Grading</h1>
    <p>87 is equvalent to <?php echo getLetterGrade(87);?></p>
    <p>94 is equvalent to <?php echo getLetterGrade(94);?></p>
    <p>74 is equvalent to <?php echo getLetterGrade(74);?></p>
</body>
</html>