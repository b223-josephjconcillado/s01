<?php

// [SECTION] Comments
// ctrl + / (single line comment)
// ctrl + shift + / (multiline comment)



// [SECTION] Variables
// variables are defined using dollar ($) notation before the name of the variable

$name = 'John Smith';
$email = 'johnsmith@mail.com';

// [SECTION] Constants
// Constants are defined using the 'define()' function.
// naming convention for "constants" should be in ALL CAPS.
// Doesn't use the $ notation before the variable.

define('PI',3.1416);


// [SECTION] Data types
// strings
$state = 'New York';
$country = 'United States of America';
$address = $state .', '.$country; // concatenation via (.) operator
$address = "$state, $country"; // using double quotes.

// integer
$age = 31;
$headcount = 26;

// floats
$grade  = 98.2;
$distanceInKilometers = 1342.12;

// boolean
$hasTravelledAbroad = false;
$haveSymptoms = true;

// array
$grades = array(98.7,92.1,90.2,94.6);

// null
$spouse = null;
$middleName = null;

// objects
$gradeObj = (object)[
    'firstGrading' => 98.7,
    'secondGrading' => 92.1,
    'thirdGrading' => 90.2,
    'fourthGrading' => 94.6
];
$personObj = (object)[
    'fullName' => 'John Smith',
    'isMarried' => false,
    'age' => 35,
    'address' => (object)[
        'state' => 'New York',
        'country' => 'United States of America'
    ]
];
// [SECTION] Operators
// Assignment Operators
$x = 1342.14;
$y = 1268.24;

$isLegalAge = true;
$isRegistered = false;

// [SECTION] Functions
function getFullName($firstName, $middleInitial, $lastName){
    return "$lastName, $firstName $middleInitial";
}

// [SECTION] If-ElseIf-Else Statement

function determineTyphoonIntensity($windSpeed){
    if($windSpeed < 30) {
        return 'Not a typhoon yet';
    } else if($windSpeed <= 61) {
        return 'Tropical Deppression Detected.';
    } else if($windSpeed >=62 && $windSpeed <= 88) {
        return 'Tropical Storm Detected.';
    } else if($windSpeed >= 89 && $windSpeed <= 117) {
        return 'Severe Tropical Storm Detected.';
    } else {
        return 'Typhoon detected.';
    }
}

// Conditional ( ternary ) operator
function isUnderAge($age){
    return ($age < 18) ? 'Legal age' : 'under age';
}

// Switch statement
function determineComputerUser($computerNumber){
    switch ($computerNumber) {
        case 1:
            return 'Linus Torvalds';
            break;
        
        case 2:
            return 'Steve Jobs';
            break;

        case 3:
            return 'Sid Meier';
            break;

        case 4:
            return 'Onel De Guzman';
            break;
         
        case 5:
            return 'Christian Salvador';
            break;

        default:
            return $computerNumber . ' is out of bounds.';
            break;
    }
}

// Try-catch
function greeting($str) {
    try{
        if(gettype($str) === 'string'){
            echo $str;
        } else {
            throw new Exception("Oops!");
        }
    } catch (Exception $e) {
        echo $e->getMessage();
    } finally {
        echo "I did it again";
    }
}


?>