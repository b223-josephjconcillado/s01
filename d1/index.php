<!-- Serving PHP files: php -S localhost:8000 -->

<!-- 
    - "code.php" is used for defining php statements and functions.
    - "index.php" for embedding php in HTML to be serve and shown in our browser.
 -->

 <?php require_once './code.php' ;?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>S01: PHP Basics and Selection Control</title>

    </head>
    <body>
        <!-- <h1>Hello World!</h1> -->
        <h1>Echoing Values</h1>
        <!-- single quote cannot output the variable values -->
        <p><?php echo 'Good day $name! Your given email is $email';?></p>
        <!-- single quote can be used but concatenation is need using . -->
        <p><?php echo 'Good day ' . $name. '! Your given email is ' . $email .'.';?></p>
        <!-- double quote -->
        <p><?php echo "Good day $name! Your given email is $email";?></p>
        <p><?php echo PI;?></p>

        <h1>Data Types</h1>
        <p><?php echo $hasTravelledAbroad;?></p>
        <p><?php echo $spouse;?></p>
        <!-- To see their types instead, we can use var_dump() function -->
        <p><?php echo gettype($hasTravelledAbroad);?></p>
        <p><?php echo gettype($spouse);?></p>

        <p><?php echo var_dump($hasTravelledAbroad);?></p>
        <p><?php echo var_dump($spouse);?></p>

        <p><?php echo var_dump($gradeObj);?></p>
        <p><?php echo $personObj->fullName;?></p>

        <p><?php echo $grades[3];?></p>

        <h1>Operators</h1>
        <p>X: <?php echo $x;?></p>
        <p>Y: <?php echo $y;?></p>
        
        <p>is legal age: <?php var_dump($isLegalAge)?></p>
        <p>is registered: <?php var_dump($isRegistered)?></p>

        <h2>Arithmetic Operators</h2>
        <p>Sum: <?php echo $x + $y;?></p>
        <p>Difference: <?php echo $x - $y;?></p>
        <p>Product: <?php echo $x * $y;?></p>
        <p>Quotient: <?php echo $x / $y;?></p>
        <p>Modulo: <?php echo $x % $y;?></p>

        <h2>Equality Operator</h2>
        <p>Loose Equality: <?php var_dump($x == '1342.14');?></p>
        <p>Strict Equality: <?php var_dump($x === '1342.14');?></p>
        <p>Loose Inequality: <?php var_dump($x != '1342.14');?></p>
        <p>Strict Inequality: <?php var_dump($x !== '1342.14');?></p>

        <h2>Greater/Lesser Operator</h2>
        <p>is lesser: <?php var_dump($x<$y);?></p>
        <p>is greater: <?php var_dump($x>$y);?></p>
        <p>is lesser or equal: <?php var_dump($x<=$y);?></p>
        <p>is greater or equal: <?php var_dump($x>=$y);?></p>

        <h2>Logical Operator</h2>
        <p>Are all requirements met: <?php var_dump($isLegalAge && $isRegistered);?></p>
        <p>Are some requirements met: <?php var_dump($isLegalAge || $isRegistered);?></p>
        <p>Are some requirements met: <?php var_dump($isLegalAge && !$isRegistered);?></p>

        <h1>Function</h1>
        <p>Full Name: <?php echo getFullName('John','D','Smith');?></p>

        <h1>Selection Control Structures</h1>
        <p>If-ElseIf-Else</p>

        <p><?php echo determineTyphoonIntensity(12);?></p>
        <h3>Ternary Sample</h3>
        <p>78: <?php echo isUnderAge(78);?></p>
        <p>17: <?php echo isUnderAge(17);?></p>

        <h2>Switch case</h2>
        <p><?php echo determineComputerUser(5);?></p>
        <p><?php echo determineComputerUser(10);?></p>

        <h2>Try Catch Finally</h2>
        <p><?php greeting("Hello ")?></p>
        <p><?php greeting(25)?></p>
    </body>
</html>